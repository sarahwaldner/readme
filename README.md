---
layout: markdown_page  
title: "Sarah Waldner's README"
---


### Sarah Waldner's README
Hello! My name is Sarah Waldner and I am the [Group Manager, Product](https://about.gitlab.com/job-families/product/product-management-leadership/#group-manager-product-gmp) for the [Create stage](https://about.gitlab.com/direction/create/) here at GitLab.

### Who I am
- My friends and family call me Sarah...everyone else also calls me Sarah because there are no clever nicknames for "Sarah"
- I live in Denver, CO with my partner, Chris, and our maine coone cat [PJ](https://about.gitlab.com/company/team-pets/#125-pj)
- I received my Bachelors of Science in Chemical Engineering from the University of Colorado at Boulder. I spent a brief time working in oil and gas before deciding I needed to pivot. After teaching myself how to code, I transitioned to technology.
- I grew up ski racing and while I no longer race, I am an avid skiier.
- My dad taught me how to mountain bike as a teenager and that is my second passion. When I am not skiing, I am bombing trails on my Yeti.

### How I operate
- I am a morning person. I start everyday at 6am with [postural restoration exercises,](https://www.posturalrestoration.com/the-science/basic-concepts-of-the-postural-restoration-institute) a cup of coffee, and 30 minutes of reading my current business book. Right now I am working through High Output Management.
- I begin work at 7am and I always start with the most complex or creative task I have on my docket. My productivity between 6am and 9am is significantly higher than the rest of the day.
- I typically work between 7am and 3pm or 4pm. I transition off of work to my evening with a workout.
- I try to stack meetings together to create longer uniterrupted spaces of work time. I will ask you to reschedule something, if possible, to achieve this.
- I work solely off of todos and issue assignments. I will clear all of my todos everyday. Issue assignments are for longer running tasks. I keep my issue assignments to 20 issues or less so that I can quickly triage them on a single page.
- After work is reserved for time with my partner, friends, bike rides, and house projects.

### Things to know about me
- My favorite GitLab values are [Efficiency](https://about.gitlab.com/handbook/values/#efficiency) and [Iteration](https://about.gitlab.com/handbook/values/#iteration). I believe decisions should be made quickly and scoped as small as possible. It is rare we come across a one-way door.
- I strongly believe in bias toward action and I will often make a decision with less information to preserve effciency and productivity.
- I am working on being more transparent. Sometimes I get [imposter syndrome](https://en.wikipedia.org/wiki/Impostor_syndrome) and I feel insecure with public channels.
- When I recognize I am wrong, I will immediately apologize.
- Earlier in my career, I was told I was unapproachable. I've actively worked to smile more and change my voice inflection to become more approachable while not sacrificing my directness and drive.
- I need time to understand and process information. You will always get my best ideas the next day.
- Injustice is a strong trigger for me. When someone takes advantage of someone else or a system, I get mad.
- I am very good at receiving critique and I pride myself on being self-aware. Please give me direct feedback. I see it as a gift and I will action it.
- While I enjoy interactions with others, it drains my battery. I am an introvert and I need time to recharge.

### Tenets I live by
These are not ground-breaking - everyone has heard these before. But when I learned what these meant in practice, they completely changed the trajectory of my life.

1. Do your best with what you have in the time that you are given.
2. Success is the sum of small efforts repeated day in and day out.
3. You cannot always control what happens to you, but you can control how you react to it.

### What I am focused on
1. Building my skills as a people manager.
2. Becoming an expert in the Create stage.
3. Developing a vision for No code, Low code.

### Personality test results
- My [SOCIAL STYLE](https://about.gitlab.com/handbook/leadership/emotional-intelligence/social-styles/#introducing-social-styles) is **Driver**.
- My 16personalities results are [Defender, ISFJ](https://www.16personalities.com/isfj-personality).
- My strengthfinder's results are Achiever, Learner, Restorative, Discipline, and Relator.
